#include "mycc.h"

///////////////////////////////////////////////////////////////////////////////////////////////////

char *arg_regs[] = {
    "rcx",
    "rdx",
    "r8",
    "r9",
};

///////////////////////////////////////////////////////////////////////////////////////////////////

int nxt_label_id = 0;

void gen_node(Node *node) ;

void gen_lval(Node *node) {
  if (node->kind == ND_LVAR) {
    printf("  mov rax, rbp\n");
    printf("  sub rax, %d\n", node->m.lval.offset);
    printf("  push rax\n");
    return;
  }
  if (node->kind == ND_DEREF) {
    gen_node( node->m.dereference.node);
    return;
  }
  error("not lval");
}

 void gen_binary_operator(Node *node) {
  gen_node(node->m.binary_operator.lhs);
  gen_node(node->m.binary_operator.rhs);

  printf("  pop rdi\n");
  printf("  pop rax\n");

  switch (node->m.binary_operator.operator_kind) {
  case OP_EQ: //==
    printf("  cmp rax, rdi\n");
    printf("  sete al\n");
    printf("  movzb rax, al\n");
    break;
  case OP_NE: //!=
    printf("  cmp rax, rdi\n");
    printf("  setne al\n");
    printf("  movzb rax, al\n");
    break;
  case OP_LT: // <
    printf("  cmp rax, rdi\n");
    printf("  setl al\n");
    printf("  movzb rax, al\n");
    break;
  case OP_LE: // <=
    printf("  cmp rax, rdi\n");
    printf("  setle al\n");
    printf("  movzb rax, al\n");
    break;
  case OP_ADD:
    printf("  add rax, rdi\n");
    break;
  case OP_SUB:
    printf("  sub rax, rdi\n");
    break;
  case OP_MUL:
    printf("  imul rax, rdi\n");
    break;
  case OP_DIV:
    printf("  cqo\n");
    printf("  idiv rdi\n");
    break;
  }

  printf("  push rax\n");
}

void gen_node(Node *node) {
  switch (node->kind) {
  case ND_RETURN:
    gen_node(node->m.return_.node);
    printf("  pop rax\n");
    printf("  mov rsp, rbp\n");
    printf("  pop rbp\n");
    printf("  ret\n");
    return;
  case ND_IF:
  {
    int label_id = nxt_label_id++;
    gen_node(node->m.if_.condition);
    printf("  pop rax\n");
    printf("  cmp rax, 0\n");
    printf("  je  .L_ifskip_%d\n", label_id);
    gen_node(node->m.if_.execution[0]);
    if (node->m.if_.execution[1])
      printf("  jmp .L_else_%d\n", label_id);
    printf(".L_ifskip_%d:\n", label_id);
    if (node->m.if_.execution[1]){
      gen_node(node->m.if_.execution[1]);
      printf(".L_else_%d:\n", label_id);
    }
    ++label_id;
    return;
  }
  case ND_WHILE:
  {
    int label_id = nxt_label_id++;
    printf(".L_begin_while_%d:\n", label_id);
    gen_node(node->m.while_.condition);
    printf("  pop rax\n");
    printf("  cmp rax, 0\n");
    printf("  je  .L_end_while_%d\n", label_id);
    gen_node(node->m.while_.execution);
    printf("  jmp .L_begin_while_%d\n", label_id);
    printf(".L_end_while_%d:\n", label_id);
    ++label_id;
    return;
  }
  case ND_FOR:
  {
    int label_id = nxt_label_id++;
    gen_node(node->m.for_.condition[0]);
    printf(".L_begin_for_%d:\n", label_id);
    gen_node(node->m.for_.condition[1]);
    printf("  pop rax\n");
    printf("  cmp rax, 0\n");
    printf("  je  .L_end_for_%d\n", label_id);
    gen_node(node->m.for_.execution);
    gen_node(node->m.for_.condition[2]);
    printf("  jmp .L_begin_for_%d\n", label_id);
    printf(".L_end_for_%d:\n", label_id);
    ++label_id;
    return;
  }
  case ND_BLOCK:
  {
    for (NodeAry *node_ary = node->m.block.node_ary; node_ary;
         node_ary = node_ary->next) {
      gen_node(node_ary->node);
      printf("  pop rax\n");
    }
    return;
  }
  case ND_NUM:
    printf("  push %d\n", node->m.num.val);
    return;
  case ND_CALL_FUNC: {
    // TODO:>関数呼び出しをする前にRSPが16の倍数になっていなければいけません。

    for (int idx = 0; idx < 4; idx++) {
      Node *arg = node->m.call_func.func_args[idx];
      if (!arg) break;
      gen_node(arg);
      printf("  pop %s\n", arg_regs[idx]);
    }

    printf("  call %.*s\n", node->m.call_func.func_len, node->m.call_func.func_str);
    printf("  push rax\n");
    return;
  }
  case ND_LVAR:
    gen_lval(node);
    printf("  pop rax\n");
    printf("  mov rax, [rax]\n");
    printf("  push rax\n");
    return;
  case ND_ASSIGN:
    gen_lval(node->m.assign.lhs);
    gen_node(node->m.assign.rhs);

    printf("  pop rdi\n");
    printf("  pop rax\n");
    printf("  mov [rax], rdi\n");
    printf("  push rdi\n");
    return;
  case ND_BINARY_OPERATOR:{
    gen_binary_operator(node);
    return;
  }
  case ND_ADDR: {
    gen_lval(node->m.address.node);
    return;
  }
  case ND_DEREF: {
    gen_node(node->m.dereference.node);
    printf("  pop rax\n");
    printf("  mov rax, [rax]\n");
    printf("  push rax\n");
    return;
  }
  }
}

///////////////////////////////////////////////////////////////////////////////////////////////////
void gen_args(Function * func) {
  LocalVariableList *variable = func->lval_ary;
  for (int idx = 0; idx < func->arg_num; idx++, variable = variable->next) {
    printf("  mov rax, rbp\n");
    printf("  sub rax, %d\n", variable->offset);
    printf("  mov [rax], %s\n", arg_regs[idx]);
  }
}

void gen_func(Function *func) {
  printf("%.*s:\n", func->name_len, func->name);

  // プロローグ
  // 変数26個分の領域を確保する
  printf("  push rbp\n");
  printf("  mov rbp, rsp\n");
  printf("  sub rsp, 208\n");

  gen_args( func );

  // 先頭の式から順にコード生成
  for (NodeAry *node_ary = func->node_ary; node_ary;
       node_ary = node_ary->next) {
    gen_node(node_ary->node);

    // 式の評価結果としてスタックに一つの値が残っている
    // はずなので、スタックが溢れないようにポップしておく
    printf("  pop rax\n");
  }

  // エピローグ
  // 最後の式の結果がRAXに残っているのでそれが返り値になる
  printf("  mov rsp, rbp\n");
  printf("  pop rbp\n");
  printf("  ret\n");
}