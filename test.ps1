
########################################################
function Assert($in, $expected) {
  ./mycc "$in" > tmp.s
  gcc -o tmp tmp.s
  ./tmp.exe
  $actual = $LASTEXITCODE

  if ( $actual -eq $expected ) {
    Write-Output "$in => $actual"
  }
  else {
    Write-Output "$in => $expected expected, but got $actual"
    exit 1
  }
}

########################################################
Assert "int main(){ return 0;}" 0  ;
Assert "int main(){ return 42;}" 42  ;
Assert "int main(){ return 3+7-9; }" 1  ;
Assert "int main(){ return 3 + 7- 9; }" 1  ;
Assert "int main(){ return 3 + 2 * 4; }" 11  ;
Assert "int main(){ return (3 + 2) * 4; }" 20  ;
Assert "int main(){ return ((3 + 2)) * (4-3); }" 5  ;
Assert "int main(){ return 100/50-1; }" 1  ;
Assert "int main(){ return -1+2; }" 1  ;
Assert "int main(){ return -1*-2; }" 2  ;
Assert "int main(){ return 3 - -2 / ( +4+ -2); }" 4  ;

Assert "int main(){ return 1==1; }" 1  ;
Assert "int main(){ return 1+1==2; }" 1  ;
Assert "int main(){ return 1==2; }" 0  ;

Assert "int main(){ return 1!=1; }" 0  ;
Assert "int main(){ return 1!=0; }" 1  ;

Assert "int main(){ return 3<2; }" 0  ;
Assert "int main(){ return 1<2; }" 1  ;

Assert "int main(){ return 3<=2; }" 0  ;
Assert "int main(){ return 1<=2; }" 1  ;
Assert "int main(){ return 3<=2+1; }" 1  ;

Assert "int main(){ return 3>2; }" 1  ;
Assert "int main(){ return 1>2; }" 0  ;

Assert "int main(){ return 3>=2; }" 1  ;
Assert "int main(){ return 1>=2; }" 0  ;
Assert "int main(){ return 3>=2+1; }" 0  ;

Assert "int main(){ int a; a=1;return a+1; }" 2  ;
Assert "int main(){ int a; a=1; int b;b=2;return b*3/(a+1); }" 3  ;
Assert "int main(){ int a=1;return a+1; }" 2  ;
Assert "int main(){ int a=1; int b=2;return b*3/(a+1); }" 3  ;

Assert "int main(){ int aa=1;int bar=2;return bar*3/(aa+1); }" 3  ;
Assert "int main(){ int hoge_5=1;int bar=2;return bar*3/(hoge_5+1); }" 3  ;

Assert "int main(){ return 3;return 2; }" 3  ;
Assert "int main(){ int return_ = 3;return return_+1; int a=2; }" 4  ;

Assert "int main(){ if(3)return 1;return 2; }" 1  ;
Assert "int main(){ if(0)return 1;return 2; }" 2  ;

Assert "int main(){ if(3)return 1;else return 2; }" 1  ;
Assert "int main(){ if(0)return 1;else return 2; }" 2  ;
Assert "int main(){ int i=0;if(i==0)return 1;else return 2; }" 1  ;

Assert "int main(){ int i=0; while( i<10 ) i=i+1; return i; }" 10 ;

Assert "int main(){ for(int i=0;i<10;i=i+1)0;return i; }" 10 ;
Assert "int main(){ int a=5;for(int i=0;i<10;i=i+1)a=a+3;return a; }" 35 ;

Assert "int main(){ {return 0;} }" 0  ;
Assert "int main(){ {int i=2;return i;} }" 2  ;
Assert "int main(){ int i=0; while( 1 ){ i=i+1; if(i>4){ return i;} } }" 4 ;

Assert "int f(){return 3;}int main(){ return f(); }" 3 ;
Assert "int f(){return 3;}int main(){ int res= f(); return res; }" 3 ;
Assert "int f(){return 3;}int g(){return 4;}int main(){ int res= f()*g(); return res; }" 12 ;

Assert "
int f(){ int aa = 111; int bb = 222; return aa + bb ; }
int g(){ int aa = 11; int bb = 22; return aa + bb ; }
int main(){ int res= f() - g() ; return res; }
" 300 ;

Assert "
int f( int  x ){ return x  ; }
int main(){ return f( 3 ); }
" 3 ;
Assert "
int f( int x ){ return x + 1 ; }
int g( int x, int y ){  return x + y ; }
int main(){ int res= f( 2 ) + g( 3, 4 ) ; return res; }
" 10 ;

Assert "
int main(){
  int x=3;
  int* p=&x;
  return *p;
}
" 3  ;

Assert "
int main(){
  int x=3;
  int* p=&x;
  *p=4;
  return *p;
}
" 4  ;

Write-Output "OK"
