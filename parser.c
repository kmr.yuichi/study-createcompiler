#include "mycc.h"

///////////////////////////////////////////////////////////////////////////////////////////////////

typedef struct TypeNameAry TypeNameAry;
struct TypeNameAry {
  char *name;
  int name_len;
  TypeNameAry *next;
};

TypeNameAry *type_ary;
bool is_type_name(char *ident, int len) {
  for (TypeNameAry *cur = type_ary; cur; cur = cur->next) {
    if ( len==cur->name_len && strncmp(ident, cur->name, len) == 0) {
      return true;
    }
  }
  return false;
}
void add_type(char *name, int len) {
  for (TypeNameAry *cur = type_ary; cur; cur = cur->next) {
    if (cur->next) {
      continue;
    }
    cur->next = calloc(1, sizeof(TypeNameAry));
    cur->next->name = name;
    cur->next->name_len = len;
    return ;
  }
}

///////////////////////////////////////////////////////////////////////////////////////////////////

// 次のトークンが期待している記号のときには、トークンを1つ読み進めて
// 真を返す。それ以外の場合には偽を返す。
bool consume(char *op) {
  if (token->kind != TK_RESERVED ||
      strlen(op) != token->len ||
      memcmp(token->str, op, token->len))
    return false;
  token = token->next;
  return true;
}

// 次のトークンが期待している記号のときには、トークンを1つ読み進める。
// それ以外の場合にはエラーを報告する。
void expect(char *op) {
  if ( !consume( op ) )
    error_at(token->str,"not a '%c'", op);
}

typedef struct Type Type;
struct Type {
  enum { OBJ, PTR } type;
  char *name;
  int name_len;
  Type *ptr_to;
}; Type *consume_type()
{
  if (!is_type_name(token->str, token->len)) return NULL;
  Type *res = calloc(1,sizeof(Type));
  res->type = OBJ;
  res->name = token->str;
  res->name_len = token->len;
  token = token->next;

  for (;;) {
    if (!consume("*")) {
      break;
    }
    Type *tmp = calloc(1, sizeof(Type));
    tmp->type = PTR;
    tmp->ptr_to = res;
    res = tmp;
  }

  return res;
}

Token *consume_ident()
{
  if (token->kind != TK_IDENT)
    return NULL;
  Token *res = token;
  token = token->next;
  return res;
}

// 次のトークンが数値の場合、トークンを1つ読み進めてその数値を返す。
// それ以外の場合にはエラーを報告する。
int expect_number() {
  if (token->kind != TK_NUM)
    error_at(token->str,"not a number");
  int val = token->val;
  token = token->next;
  return val;
}

///////////////////////////////////////////////////////////////////////////////////////////////////

Node *new_node(NodeKind kind) {
  Node *node = calloc(1, sizeof(Node));
  node->kind = kind;
  return node;
}

Node *new_node_assign(Node *lhs, Node *rhs) {
  Node *node = calloc(1, sizeof(Node));
  node->kind = ND_ASSIGN;
  node->m.assign.lhs = lhs;
  node->m.assign.rhs = rhs;
  return node;
}

Node *new_node_binary_operator(BinaryOperatorKind kind, Node *lhs, Node *rhs) {
  Node *node = calloc(1, sizeof(Node));
  node->kind = ND_BINARY_OPERATOR;
  node->m.binary_operator.lhs = lhs;
  node->m.binary_operator.rhs = rhs;
  node->m.binary_operator.operator_kind = kind;
  return node;
}

Function *cur_func;

LocalVariableList *is_variable(Token *token) {
  for (LocalVariableList *lval = cur_func->lval_ary; lval; lval = lval->next)
    if (lval->len == token->len && !memcmp(token->str, lval->name, lval->len))
      return lval;
  return NULL;
}
void add_variable(Token *token){
  LocalVariableList *lvar = calloc(1, sizeof(LocalVariableList));
  int cur_offset = cur_func->lval_ary ? cur_func->lval_ary->offset : 0;
  lvar->name = token->str;
  lvar->len = token->len;
  lvar->offset = cur_offset + 8;

  if(!cur_func->lval_ary ){
    cur_func->lval_ary = lvar;
  } else {
    cur_func->lval_ary->next = lvar;
  }
}

Node *new_node_call_func(Token *token) {
  Node *node = calloc(1, sizeof(Node));
  node->kind = ND_CALL_FUNC;
  node->m.call_func.func_str = token->str;
  node->m.call_func.func_len = token->len;
  return node;
}

Node *new_node_idnt(Token *token) {
  Node *node = calloc(1, sizeof(Node));
  node->kind = ND_LVAR;
  LocalVariableList *var = is_variable(token);
  if (!var) error_at(token->str, "variable not defined");
  node->m.lval.offset = var->offset;
  return node;
}

Node *new_node_num(int val) {
  Node *node = calloc(1, sizeof(Node));
  node->kind = ND_NUM;
  node->m.num.val = val;
  return node;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
bool at_eof() {
  return token->kind == TK_EOF;
}

///////////////////////////////////////////////////////////////////////////////////////////////////

Node *stmt();
Function *function();
Node *expr();
Node *assign();
Node *equality();
Node *relational();
Node *add();
Node *mul();
Node *unary();
Node *primary();

Node *code[100];
Program *program()
{
  // program = function*

  if (!type_ary) {
    // init
    type_ary = calloc(1, sizeof(TypeNameAry));
    type_ary->name = "int";
    type_ary->name_len = 3;
    add_type("void", strlen("void"));
  }

  Program *program = calloc(1, sizeof(Program));
  program->func_ary = calloc(1, sizeof(FunctionAry));

  FunctionAry *list_last = program->func_ary;
  while (true) {
    list_last->function = function();
    if (at_eof()) {
      break;
    }

    list_last->next = calloc(1, sizeof(FunctionAry));
    list_last = list_last->next;
  }
  list_last->next = NULL;
  return program;
}

Function *function()
{
  // function
  //   = type ident "("")""{" stmt* "}"
  //   | type ident "(" type ident ( "," type ident )? ")""{" stmt* "}"

  Type *ret_type = consume_type();
  if (!ret_type) error_at(token->str, "not type.");

  Token *ident = consume_ident();
  if (!ident) error_at(token->str, "not func name.");
  if (is_type_name(ident->str, ident->len)) error_at(token->str, "it's type.");

  cur_func = calloc(1, sizeof(Function));
  cur_func->name = ident->str;
  cur_func->name_len = ident->len;
  cur_func->node_ary = calloc(1, sizeof(NodeAry));
  expect("(");

  do {
    Type *arg_type = consume_type();
    if (!arg_type) break;

    Token *arg = consume_ident();
    if (!arg) error_at(token->str, "not func arg.");
    if (is_type_name(arg->str, arg->len)) error_at(token->str, "it's type.");

    cur_func->arg_num++;
    add_variable(arg);
  } while (consume(","));

  expect(")");
  expect("{");

  NodeAry *list_last = cur_func->node_ary;
  while (true) {
    list_last->node = stmt();
    if ( consume("}") || at_eof()) {
      break;
    }

    list_last->next = calloc(1, sizeof(NodeAry));
    list_last = list_last->next;
  }
  list_last->next = NULL;

  return cur_func;
}

Node *stmt() {
  // stmt    = expr ";"
  //         | "{" stmt* "}"
  //         | return expr ";"
  //         | "if" "(" expr ")" stmt ("else" stmt)?
  //         | "while" "(" expr ")" stmt
  //         | "for" "(" expr? ";" expr? ";" expr? ")" stmt
  Node *node;

  if (consume("return")) {
    node = new_node(ND_RETURN);
    node->m.return_.node = expr();
    expect(";");
  } else if (consume("if")) {
    node = new_node(ND_IF);
    expect("(");
    node->m.if_.condition = expr();
    expect(")");
    node->m.if_.execution[0] = stmt();
    node->m.if_.execution[1] = NULL;
    if (consume("else"))
      node->m.if_.execution[1] = stmt();
  } else if (consume("while")) {
    node = new_node(ND_WHILE);
    expect("(");
    node->m.while_.condition = expr();
    expect(")");
    node->m.while_.execution = stmt();
  } else if (consume("for")) {
    node = new_node(ND_FOR);
    expect("(");
    // 条件が空の場合は、とりあえず未対応で…。
    node->m.for_.condition[0] = expr();
    expect(";");
    node->m.for_.condition[1] = expr();
    expect(";");
    node->m.for_.condition[2] = expr();
    expect(")");
    node->m.for_.execution = stmt();
  } else if (consume("{")) {
    node = new_node(ND_BLOCK);
    NodeAry *head = calloc(1, sizeof(NodeAry));
    NodeAry *ary_last = head;
    for (;;) {
      if (consume("}")) break;
      ary_last->next = calloc(1, sizeof(NodeAry));
      ary_last->next->node = stmt();
      ary_last = ary_last->next;
    }
    node->m.block.node_ary = head->next;
    return node;
  } else {
    node = expr();
    expect(";");
  }

  return node;
}

Node *expr() {
  // expr = assign
  return assign();
}

Node *assign(){
  // assign     = equality ("=" assign)?
  Node *node = equality();

  for (;;) {
    if (consume("="))
      node = new_node_assign(node, assign());
    return node;
  }
}

Node *equality()
{
  // equality = relational ("==" relational | "!=" relational)*
  //          | def_val

  Type *type = consume_type();
  if (type) {  // キャストとの区別は？？
    Token *var = consume_ident();
    if (!var) error_at(type->name,"variable not defined.");
    if(is_type_name(var->str,var->len)) error_at(var->str,"it's type name.");
    if(is_variable(var)) error_at(var->str,"already defined.");

    add_variable(var);

    return new_node_idnt(var);
  }

  Node *node = relational();

  for (;;) {
    if (consume("=="))
      node = new_node_binary_operator(OP_EQ, node, mul());
    else if (consume("!="))
      node = new_node_binary_operator(OP_NE, node, mul());
    else
      return node;
  }
}

Node *relational()
{
  // relational = add ("<" add | "<=" add | ">" add | ">=" add)*
  Node *node = add();

  for (;;) {
    if (consume("<"))
      node = new_node_binary_operator(OP_LT, node, add());
    else if (consume(">="))
      node = new_node_binary_operator(OP_LT, add(), node);
    else if (consume("<="))
      node = new_node_binary_operator(OP_LE, node, add());
    else if (consume(">"))
      node = new_node_binary_operator(OP_LE, add(), node);
    else
      return node;
  }
}

Node *add() {
  // add        = mul ("+" mul | "-" mul)*
  Node *node = mul();

  for (;;) {
    if (consume("+"))
      node = new_node_binary_operator(OP_ADD, node, mul());
    else if (consume("-"))
      node = new_node_binary_operator(OP_SUB, node, mul());
    else
      return node;
  }
}

Node *mul() {
  // mul        = unary ("*" unary | "/" unary)*
  Node *node = unary();

  for (;;) {
    if (consume("*"))
      node = new_node_binary_operator(OP_MUL, node, unary());
    else if (consume("/"))
      node = new_node_binary_operator(OP_DIV, node, unary());
    else
      return node;
  }
}

Node *unary() {
  // unary = ("+" | "-")? primary
  //       | ("*"|"&") unary

  if (consume("+"))
    return primary();
  if (consume("-"))
    return new_node_binary_operator(OP_SUB, new_node_num(0), primary());
  if (consume("&")) {
    Node *node = new_node(ND_ADDR);
    node->m.address.node = unary();
    return node;
  }
  if (consume("*")) {
    Node *node = new_node(ND_DEREF);
    node->m.dereference.node = unary();
    return node;
  }
  return primary();
}

Node *primary() {
  // primary
  //   = num
  //   | ident
  //   | ident "(" ")"
  //   | ident "(" (expr ",")? expr ")"
  //   | "(" expr ")"
  // 次のトークンが"("なら、"(" expr ")"のはず
  if (consume("(")) {
    Node *node = expr();
    expect(")");
    return node;
  }

  Token *ident = consume_ident();
  if (ident) {
    if (consume("(")) {
      Node *node = new_node_call_func(ident);
      if (consume(")")) {
        node->m.call_func.func_args[0] = NULL;
      } else {
        for (int idx = 0;; idx++) {
          if (idx >= 4) error_at(node->m.call_func.func_str,"too many args");

          node->m.call_func.func_args[idx] = expr();
          if (consume(")")) {
            node->m.call_func.func_args[idx + 1] = NULL;
            break;
          } else {
            expect(",");
          }
        }
      }
      return node;
    } else {
      return new_node_idnt(ident);
    }
  } else {
    // そうでなければ数値のはず
    return new_node_num(expect_number());
  }
}
