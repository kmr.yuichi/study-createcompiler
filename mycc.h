#include <ctype.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


///////////////////////////////////////////////////////////////////////////////////////////////////

// 入力プログラム
extern char *user_input;

// エラー箇所を報告する
void error_at(char *loc, char *fmt, ...) ;

// エラーを報告するための関数
// printfと同じ引数を取る
void error(char *fmt, ...) ;

///////////////////////////////////////////////////////////////////////////////////////////////////

typedef struct Node Node;

typedef struct NodeAry NodeAry;
struct NodeAry {
  Node *node;
  NodeAry *next;
};

// 抽象構文木のノードの種類
typedef enum {
  ND_ASSIGN, // =
  ND_BINARY_OPERATOR, // 二項演算子
  ND_RETURN, // return
  ND_IF, // if
  ND_WHILE, // while
  ND_FOR, // for
  ND_BLOCK, // ブロック
  ND_CALL_FUNC, // 関数呼び出し
  ND_LVAR,   // ローカル変数
  ND_NUM, // 整数
  ND_ADDR, // アドレス演算子&
  ND_DEREF, // 間接演算子*
} NodeKind;

typedef enum {
  OP_EQ, //==
  OP_NE, //!=
  OP_LT,  // <
  OP_LE,  // <=
  OP_ADD, // +
  OP_SUB, // -
  OP_MUL, // *
  OP_DIV, // /
} BinaryOperatorKind;


// 抽象構文木のノードの型
typedef struct AssignNode AssignNode;
struct AssignNode {
  Node *lhs;     // 左辺
  Node *rhs;     // 右辺
};
typedef struct BinaryOperatorNode BinaryOperatorNode;
struct BinaryOperatorNode {
  BinaryOperatorKind operator_kind ;
  Node *lhs;     // 左辺
  Node *rhs;     // 右辺
};
typedef struct ReturnNode ReturnNode;
struct ReturnNode{
  Node *node;
};
typedef struct IfNode IfNode;
struct IfNode{
  Node * condition; // 制御構文,丸括弧内
  Node * execution[2]; // 制御構文,波括弧内
};
typedef struct WhileNode WhileNode;
struct WhileNode{
  Node * condition; // 制御構文,丸括弧内
  Node * execution; // 制御構文,波括弧内
};
typedef struct ForNode ForNode;
struct ForNode{
  Node * condition[3]; // 制御構文,丸括弧内
  Node * execution; // 制御構文,波括弧内
};
typedef struct BlockNode BlockNode;
struct BlockNode{
  NodeAry *node_ary;
};
typedef struct CallFuncNode CallFuncNode;
struct CallFuncNode{
  char *func_str;// トークン文字列
  int func_len;  // トークンの長さ
  Node *func_args[5];
};
typedef struct LValNode LValNode;
struct LValNode{
  int offset;
};
typedef struct NumNode NumNode;
struct NumNode{
  int val;
};
typedef struct AddressNode AddressNode;
struct AddressNode{
  Node *node;
};
typedef struct DeReferenceNode DeReferenceNode;
struct DeReferenceNode{
  Node *node;
};
struct Node {
  NodeKind kind;  // ノードの型
  union {
    AssignNode assign;
    BinaryOperatorNode binary_operator;
    ReturnNode return_;
    IfNode if_;
    WhileNode while_;
    ForNode for_;
    BlockNode block;
    CallFuncNode call_func;
    LValNode lval;
    NumNode num;
    AddressNode address;
    DeReferenceNode dereference;
  } m;
};

///////////////////////////////////////////////////////////////////////////////////////////////////

// トークンの種類
typedef enum {
  TK_RESERVED, // 記号
  TK_IDENT,    // 識別子
  TK_NUM,      // 整数トークン
  TK_EOF,      // 入力の終わりを表すトークン
} TokenKind;

typedef struct Token Token;

// トークン型
struct Token {
  TokenKind kind; // トークンの型
  Token *next;    // 次の入力トークン
  int val;        // kindがTK_NUMの場合、その数値
  char *str;      // トークン文字列
  int len;        // トークンの長さ
};

extern Token *token;

Token *tokenize(char *p) ;

///////////////////////////////////////////////////////////////////////////////////////////////////

// ローカル変数の型
typedef struct LocalVariableList LocalVariableList;
struct LocalVariableList {
  LocalVariableList *next; // 次の変数かNULL
  char *name; // 変数の名前
  int len;    // 名前の長さ
  int offset; // RBPからのオフセット
};

typedef struct Function Function;
struct Function {
  char* name;
  int name_len;
  NodeAry *node_ary;
  LocalVariableList *lval_ary; // 引数含む,最初は引数
  int arg_num;
};

typedef struct FunctionAry FunctionAry;
struct FunctionAry {
  Function *function;
  FunctionAry *next;
};

typedef struct Program Program;
struct Program {
  FunctionAry *func_ary;
};

Program *program() ;

void gen_func(Function *func) ;
