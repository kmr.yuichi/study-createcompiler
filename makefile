CFLAGS=-std=c11 -g -static

CC := gcc

SRCS=$(wildcard *.c)
OBJS=$(SRCS:.c=.o)


mycc: $(OBJS)
	$(CC) -o mycc $(OBJS) $(LDFLAGS)

$(OBJS): mycc.h

test: mycc
	"C:\Program Files\PowerShell\7\pwsh.exe" ./test.ps1

clean:
	rm -f mycc.exe *.o *~ tmp*

.PHONY: test clean
