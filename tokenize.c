#include <ctype.h>
#include <stdbool.h>
#include <string.h>

#include "mycc.h"


///////////////////////////////////////////////////////////////////////////////////////////////////

int is_symbol(char* p) {
  // シフト代入演算子
  if (strncmp(p, "<<=", 3) == 0 || strncmp(p, ">>=", 3) == 0) {
    return 3;
  }


  // 比較演算子
  if (strncmp(p, "==", 2) == 0 || strncmp(p, "!=", 2) == 0 ||
      strncmp(p, "<=", 2) == 0 || strncmp(p, ">=", 2) == 0) {
    return 2;
  }

  // 論理演算子
  if (strncmp(p, "&&", 2) == 0 || strncmp(p, "||", 2) == 0) {
    return 2;
  }

  // 増分演算子,減分演算子
  if (strncmp(p, "++", 2) == 0 || strncmp(p, "--", 2) == 0) {
    return 2;
  }

  // アロー演算子
  if (strncmp(p, "->", 2) == 0) {
    return 2;
  }

  // XX代入演算子
  if (strncmp(p, "+=", 2) == 0 || strncmp(p, "-=", 2) == 0 ||
      strncmp(p, "*=", 2) == 0 || strncmp(p, "/=", 2) == 0 ||
      strncmp(p, "%%=", 2) == 0 || strncmp(p, "&=", 2) == 0 ||
      strncmp(p, "|=", 2) == 0 || strncmp(p, "^=", 2) == 0) {
    return 2;
  }

  // ビット演算子
  if (strncmp(p, "<<", 2) == 0 || strncmp(p, ">>", 2) == 0) {
    return 2;
  }


  // 算術演算子,間接演算子
  if (*p == '+' || *p == '-' || *p == '*' || *p == '/' || *p == '%') {
    return 1;
  }

  // 論理演算子
  if (*p == '!') {
    return 1;
  }

  // 条件演算子
  if (*p == '?' || *p == ':') {
    return 1;
  }

  // 比較演算子
  if (*p == '<' || *p == '>') {
    return 1;
  }

  // ビット演算子,アドレス演算子
  if (*p == '&' || *p == '|' || *p == '^' || *p == '~') {
    return 1;
  }

  // 括弧
  if (*p == '(' || *p == ')' || *p == '{' || *p == '}') {
    return 1;
  }

  if (*p == ',' || *p == '=' || *p == ';') {
    return 1;
  }

  return 0;
}

///////////////////////////////////////////////////////////////////////////////////////////////////

char *key_word_ary[] = {
   "auto",     "break",  "case",    "char",   "const",    "continue",
    "default",  "do",     "double",  "else",   "enum",     "extern",
    "float",    "for",    "goto",    "if",     "int",      "long",
    "register", "return", "signed",  "sizeof", "short",    "static",
    "struct",   "switch", "typedef", "union",  "unsigned", "void",
    "volatile", "while",
};

///////////////////////////////////////////////////////////////////////////////////////////////////

int valid_for_ident(char str)
{
  if ('a' <= str && str <= 'z') return true ;
  if ('A' <= str && str <= 'Z') return true ;
  if ('0' <= str && str <= '9') return true ;
  if (str == '_') return true ;
  return false;
}

int ident_len(char *str)
{
  for (int i = 0;; i++, str++)
  {
    if (!valid_for_ident(*str))
      return i;
  }
}

int is_key_word(char* p) {
  int key_word_num = sizeof(key_word_ary) / sizeof(key_word_ary[0]);
  for (int idx = 0; idx < key_word_num; idx++) {
    char *key_word = key_word_ary[idx];

    int len = strlen(key_word);
    if( strncmp(p, key_word, len) == 0 && !valid_for_ident(p[len])){
      return len;
    }
  }

  return 0;
}

///////////////////////////////////////////////////////////////////////////////////////////////////

// 現在着目しているトークン
Token *token;

// 新しいトークンを作成してcurに繋げる
Token *new_token(TokenKind kind, Token *cur, char *str, int len) {
  Token *tok = calloc(1, sizeof(Token));
  tok->kind = kind;
  tok->str = str;
  tok->len = len;
  cur->next = tok;
  return tok;
}

// 入力文字列pをトークナイズしてそれを返す
Token *tokenize(char *p) {
  Token head;
  head.next = NULL;
  Token *cur = &head;

  while (*p) {
    // 空白文字をスキップ
    if (isspace(*p)) {
      p++;
      continue;
    }

    int len_symbol =is_symbol(p);
    if (len_symbol != 0) {
      cur = new_token(TK_RESERVED, cur, p, len_symbol);
      p += len_symbol;
      continue;
    }

    int len_key_word = is_key_word(p);
    if (len_key_word != 0) {
      cur = new_token(TK_RESERVED, cur, p, len_key_word);
      p += len_key_word;
      continue;
    }

    if ('a' <= *p && *p <= 'z')
    {
      int len = ident_len(p);
      if (len <= 0)
        error("invalid ident len.");
      cur = new_token(TK_IDENT, cur, p, len);
      p += len;
      continue;
    }

    if (isdigit(*p)) {
      cur = new_token(TK_NUM, cur, p, 0);
      cur->val = strtol(p, &p, 10);
      continue;
    }

    error_at(p,"torknize failed");
  }

  new_token(TK_EOF, cur, p, 0);
  return head.next;
}

