#include "mycc.h"

///////////////////////////////////////////////////////////////////////////////////////////////////

char *user_input;

// エラー箇所を報告する
void error_at(char *loc, char *fmt, ...) {
  va_list ap;
  va_start(ap, fmt);

  int pos = loc - user_input;
  fprintf(stderr, "%s\n", user_input);
  fprintf(stderr, "%*s", pos, " "); // pos個の空白を出力
  fprintf(stderr, "^ ");
  vfprintf(stderr, fmt, ap);
  fprintf(stderr, "\n");
  exit(1);
}
// エラーを報告するための関数
// printfと同じ引数を取る
void error(char *fmt, ...) {
  va_list ap;
  va_start(ap, fmt);
  vfprintf(stderr, fmt, ap);
  fprintf(stderr, "\n");
  exit(1);
}

///////////////////////////////////////////////////////////////////////////////////////////////////

int main(int argc, char **argv) {
  if (argc != 2) {
    error("引数の個数が正しくありません");
    return 1;
  }

  user_input = argv[1];

  // トークナイズしてパースする
  // 結果はcodeに保存される
  token = tokenize(user_input);
  Program *pgm = program();

  // アセンブリの前半部分を出力
  printf(".intel_syntax noprefix\n");
  printf(".globl main\n");
  for (FunctionAry *func_ary = pgm->func_ary; func_ary; func_ary = func_ary->next) {
    Function *func = func_ary->function;
    gen_func(func);
  }

  return 0;
}
